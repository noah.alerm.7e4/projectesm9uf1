package cat.itb.m9.uf1.hashing;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Scanner;

public class Exercise1 {
    //METHODS
    /**
     * This method is used to cipher a password.
     */
    public static String Cipher(String sha, String password) {
        //CIPHERED PASSWORD
        StringBuilder sb = new StringBuilder();

        //CIPHERING
        try {
            MessageDigest md = MessageDigest.getInstance(sha);
            byte[] hashInBytes = md.digest(password.getBytes(StandardCharsets.UTF_8));

            for (byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace(); }

        return sb.toString();
    }

    /**
     * This method is used to decipher a password.
     * @param sha SHA used to cipher the password
     * @param hash Hash to decipher
     */
    public static void Decipher(String sha, String hash) {
        //POSSIBLE CHARACTERS
        char[] possibleChars = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
                'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
                'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4',
                '5', '6', '7', '8', '9'};

        StringBuilder sb = new StringBuilder();

        //DECIPHERING
        for (int i = 0; i < 62; i++) {
            for (int j = 0; j < 62; j++) {
                for (int k = 0; k < 62; k++) {
                    sb.append(possibleChars[i]);
                    sb.append(possibleChars[j]);
                    sb.append(possibleChars[k]);

                    if (Cipher(sha, sb.toString()).equals(hash))
                        break;
                    else
                        sb.setLength(0);
                }
                if (Cipher(sha, sb.toString()).equals(hash))
                    break;
            }
            if (Cipher(sha, sb.toString()).equals(hash))
                break;
        }

        System.out.printf("\u001B[34mOriginal password:\u001B[0m %s\n", sb);
    }

    //MAIN
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //MENU LOOP
        while (true) {
            System.out.println("\u001B[35m==================================================");
            System.out.println("               Exercise 1 (HASHING)               ");
            System.out.println("==================================================\u001B[0m");
            System.out.println("\u001B[34mWhat do you wish to do?\u001B[0m");
            System.out.println("1 - Cipher a password");
            System.out.println("2 - Decipher a password");
            System.out.println("\u001B[31m0 - EXIT\u001B[0m\n");

            int option = sc.nextInt();
            sc.nextLine();

            if (option == 0)
                break;
            else if (option == 1) {
                //INPUT
                System.out.println("Enter the password to cipher: ");
                String password = sc.nextLine();

                System.out.println();

                //OUTPUT
                System.out.println("\u001B[34mSHA-1: \u001B[0m" + Cipher("SHA-1", password));
                System.out.println("\u001B[34mSHA-256: \u001B[0m" + Cipher("SHA-256", password));
                System.out.println("\u001B[34mSHA3-256: \u001B[0m" + Cipher("SHA3-256", password));
            }
            else if (option == 2) {

                //INPUT
                System.out.println("Enter the hash to decipher: ");
                String hash = sc.nextLine();
                System.out.println("Enter the SHA used to cipher it: ");
                String sha = sc.nextLine();

                System.out.println();

                //INITIAL TIME
                LocalDateTime init = LocalDateTime.now();

                //OUTPUT
                Decipher(sha, hash);

                //FINAL TIME
                LocalDateTime end = LocalDateTime.now();

                Duration res = Duration.between(init, end);

                System.out.printf("\u001B[36mIt took %s seconds to decipher this hash.\u001B[0m\n", res.getSeconds());
            }

            System.out.println();
        }
    }
}

//RESULT
/*
SHA-1 --> 40 characters, 0 - 1 seconds
SHA-256 --> 64 characters, 0 - 2 seconds
SHA3-256 --> 64 characters, 0 - 2 seconds
*/
