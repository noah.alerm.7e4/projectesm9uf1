package cat.itb.m9.uf1.hashing;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Exercise3 {
    //METHODS
    /**
     * This method is used to cipher a given file.
     * @param name Picture's name
     * @param file File path (FILE)
     */
    public static void cipherPicture(String name, File file)  {
        //SHA3-256
        try {
            MessageDigest md = MessageDigest.getInstance("SHA3-256");
            byte[] hashInBytes = md.digest(Files.readAllBytes(file.toPath()));
            StringBuilder sb = new StringBuilder();

            for (byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }

            //OUTPUT
            System.out.printf("\u001B[34m%s:\u001B[0m %s\n", name, sb);
        } catch (NoSuchAlgorithmException | IOException e) {
            e.printStackTrace(); }
    }

    //MAIN
    public static void main(String[] args) {
        //FILES
        File c1 = new File("img/c1.jpg");
        File c2 = new File("img/c2.jpg");
        File c3 = new File("img/c3.jpg");

        //OUTPUTS
        cipherPicture("C1", c1);
        cipherPicture("C2", c2);
        cipherPicture("C3", c3);
    }
}

//RESULT
/*
HASH (SHA3-256): da2797aef5b080b703f2ee53768b9767fea37d1e7e50f0c7a0e339899b4775d0
PICTURE: C3 (La persistència de la memòria - Salvador Dalí)
*/

//PICTURES
/*
C1 --> Guernica - Pablo Picasso
C2 --> Las Meninas - Diego Velázquez
C3 --> La persistència del temps - Salvador Dalí
*/
