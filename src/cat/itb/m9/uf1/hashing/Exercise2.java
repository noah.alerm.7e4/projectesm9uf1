package cat.itb.m9.uf1.hashing;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class Exercise2 {
    //METHODS
    /**
     * This method is used to cipher a given file.
     * @param file File path (FILE)
     */
    public static String cipherFile(File file)  {
        //HASH
        StringBuilder sb = new StringBuilder();

        //SHA-1
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] hashInBytes = md.digest(Files.readAllBytes(file.toPath()));

            for (byte b : hashInBytes) {
                sb.append(String.format("%02x", b));
            }
        } catch (NoSuchAlgorithmException | IOException e) {
            e.printStackTrace(); }

        return sb.toString();
    }

    /**
     * This method is used to decipher the hash of a file.
     * @param hash Hash to decipher
     */
    public static void decipherFile(String hash) {
        for (int i = 1; i < 4; i++) {
            File file = new File("files/f"+i);

            if (cipherFile(file).equals(hash)) {
                System.out.println("\n\u001B[35mOriginal file: \u001B[0m" + file);
                break;
            }
        }
    }

    //MAIN
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //FILES
        File f1 = new File("files/f1");
        File f2 = new File("files/f2");
        File f3 = new File("files/f3");

        //CIPHERING
        System.out.printf("\u001B[34m%s:\u001B[0m %s\n", f1, cipherFile(f1));
        System.out.printf("\u001B[34m%s:\u001B[0m %s\n", f2, cipherFile(f2));
        System.out.printf("\u001B[34m%s:\u001B[0m %s\n\n", f3, cipherFile(f3));

        //INPUT
        System.out.println("\u001B[34mEnter one of the hashes above: \u001B[0m");
        String hash = sc.nextLine();

        //DECIPHERING
        decipherFile(hash);
    }
}
