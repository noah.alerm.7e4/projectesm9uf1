package cat.itb.m9.uf1.symmetric;

import java.util.Scanner;

public class Exercise3 {
    //METHODS
    /**
     * This method is used to cipher a given plain text through the Polibi algorithm.
     * @param plainText Text to be ciphered.
     */
    public static void cipherPlainText(String plainText) {
        //MESSAGE
        StringBuilder message = new StringBuilder();

        for (int i = 0; i < plainText.length(); i++) {
            //CURRENT CHAR
            char c = plainText.toUpperCase().charAt(i);

            //NEW NUMBER
            int num = 0;

            //CIPHERING
            switch (c){
                case 'A': num = 11; break;
                case 'B': num = 12; break;
                case 'C': num = 13; break;
                case 'Ç': num = 14; break;
                case 'D': num = 15; break;
                case 'E': num = 16; break;
                case 'F': num = 17; break;
                case 'G': num = 21; break;
                case 'H': num = 22; break;
                case 'I': num = 23; break;
                case 'J': num = 24; break;
                case 'K': num = 25; break;
                case 'L': num = 26; break;
                case 'M': num = 27; break;
                case 'N': num = 31; break;
                case 'Ñ': num = 32; break;
                case 'O': num = 33; break;
                case 'P': num = 34; break;
                case 'Q': num = 35; break;
                case 'R': num = 36; break;
                case 'S': num = 37; break;
                case 'T': num = 41; break;
                case 'U': num = 42; break;
                case 'V': num = 43; break;
                case 'W': num = 44; break;
                case 'X': num = 45; break;
                case 'Y': num = 46; break;
                case 'Z': num = 47; break;
                case '0': num = 51; break;
                case '1': num = 52; break;
                case '2': num = 53; break;
                case '3': num = 54; break;
                case '4': num = 55; break;
                case '5': num = 56; break;
                case '6': num = 57; break;
                case '7': num = 61; break;
                case '8': num = 62; break;
                case '9': num = 63; break;
                case '-': num = 64; break;
                case '+': num = 65; break;
                case '*': num = 66; break;
                case '/': num = 67; break;
                case '.': num = 71; break;
                case ',': num = 72; break;
                case ' ': num = 73; break;
                case '?': num = 74; break;
                case '¿': num = 75; break;
                case '!': num = 76; break;
                case '¡': num = 77; break;
            }

            message.append(num).append(" ");
        }

        System.out.println("CIPHERED MESSAGE: " + message);
    }

    /**
     * This method is used to decipher a message through the Polibi algorithm.
     * @param message Message to be deciphered.
     */
    public static void decipherMessage(String message) {
        //NUMBERS
        String[] numbers = message.split(" ");

        //PLAIN TEXT
        StringBuilder plainText = new StringBuilder();

        for (String num : numbers) {
            //NEW CHAR
            char c = ' ';

            //DECIPHERING
            switch (num) {
                case "11": c = 'A'; break;
                case "12": c = 'B'; break;
                case "13": c = 'C'; break;
                case "14": c = 'Ç'; break;
                case "15": c = 'D'; break;
                case "16": c = 'E'; break;
                case "17": c = 'F'; break;
                case "21": c = 'G'; break;
                case "22": c = 'H'; break;
                case "23": c = 'I'; break;
                case "24": c = 'J'; break;
                case "25": c = 'K'; break;
                case "26": c = 'L'; break;
                case "27": c = 'M'; break;
                case "31": c = 'N'; break;
                case "32": c = 'Ñ'; break;
                case "33": c = 'O'; break;
                case "34": c = 'P'; break;
                case "35": c = 'Q'; break;
                case "36": c = 'R'; break;
                case "37": c = 'S'; break;
                case "41": c = 'T'; break;
                case "42": c = 'U'; break;
                case "43": c = 'V'; break;
                case "44": c = 'W'; break;
                case "45": c = 'X'; break;
                case "46": c = 'Y'; break;
                case "47": c = 'Z'; break;
                case "51": c = '0'; break;
                case "52": c = '1'; break;
                case "53": c = '2'; break;
                case "54": c = '3'; break;
                case "55": c = '4'; break;
                case "56": c = '5'; break;
                case "57": c = '6'; break;
                case "61": c = '7'; break;
                case "62": c = '8'; break;
                case "63": c = '9'; break;
                case "64": c = '-'; break;
                case "65": c = '+'; break;
                case "66": c = '*'; break;
                case "67": c = '/'; break;
                case "71": c = '.'; break;
                case "72": c = ','; break;
                case "73": c = ' '; break;
                case "74": c = '?'; break;
                case "75": c = '¿'; break;
                case "76": c = '!'; break;
                case "77": c = '¡'; break;
            }

            plainText.append(c);
        }

        System.out.println("DECIPHERED TEXT: " + plainText);
    }

    //MAIN
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //MENU LOOP
        while (true) {
            System.out.println("\u001B[35m==================================================");
            System.out.println("                       MENU                       ");
            System.out.println("==================================================\u001B[0m");
            System.out.println("\u001B[34mWhat do you wish to do?\u001B[0m");
            System.out.println("1 - Cipher a plain text");
            System.out.println("2 - Decipher a message");
            System.out.println("\u001B[31m0 - EXIT\u001B[0m\n");

            int option = sc.nextInt();
            sc.nextLine();

            if (option == 0)
                break;
            else if (option == 1) {
                System.out.println("\u001B[34mEnter the plain text to be ciphered: \u001B[0m");
                cipherPlainText(sc.nextLine());
            }
            else if (option == 2) {
                System.out.println("\u001B[34mEnter the message to be deciphered: \u001B[0m");
                decipherMessage(sc.nextLine());
            }

            System.out.println();
        }
    }
}

//RESULT 1
/*
PLAIN TEXT: Bona tarda i bona sort
ENCRYPTED MESSAGE: 12 33 31 11 73 41 11 36 15 11 73 23 73 12 33 31 11 73 37 33 36 41
*/
//RESULT 2
/*
ENCRYPTED MESSAGE: 12 33 31 11 73 41 11 36 15 11 73 11 73 41 33 41 22 33 27 76
PLAIN TEXT: Bona tarda a tothom!
*/
