package cat.itb.m9.uf1.symmetric;

public class Exercise2 {
    //MAIN
    public static void main(String[] args) {
        //ENCRYPTED MESSAGE
        System.out.println("\u001B[34mThe message to decipher is 'cebtenzzvat, zbgureshpxre'.\u001B[0m\n");
        String message = "cebtenzzvat, zbgureshpxre";

        //CODE LOOP
        for (int i = 1; i < 25; i++) {
            StringBuilder plainText = new StringBuilder();

            for(int j = 0; j < message.length(); j++) {
                //CURRENT CHAR
                char currentChar = message.charAt(j);
                boolean isUpperCase = Character.isUpperCase(currentChar);

                //DECIPHERING
                if (Character.isLetter(message.charAt(j))) {
                    currentChar = (char) (message.charAt(j) - i);

                    if (currentChar < 'a' && !isUpperCase)
                        currentChar = (char) (currentChar + 26);
                    else if (currentChar > 'z')
                        currentChar = (char) (currentChar - 26);
                    else if (currentChar < 'A')
                        currentChar = (char) (currentChar + 26);
                    else if (currentChar > 'Z' && isUpperCase)
                        currentChar = (char) (currentChar - 26);
                }

                //DECIPHERED TEXT
                plainText.append(currentChar);
            }

            //OUTPUT
            System.out.printf("\u001B[35mCode:\u001B[0m %2d\u001B[35m, Plain Text:\u001B[0m %s\n", i, plainText);
        }
    }
}

//RESULT
/*
ENCRYPTED MESSAGE: cebtenzzvat, zbgureshpxre
PLAIN TEXT: programming, motherfucker
CODE: 13
*/
