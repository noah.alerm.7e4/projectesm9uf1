package cat.itb.m9.uf1.symmetric.aes;

import java.util.Scanner;

public class Exercise5 {
    //MAIN
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //INPUT
        System.out.println("Enter an encrypted message to be decrypted with Base64: ");
        String text = sc.nextLine();
        System.out.println("Enter the password that was used when encrypting the message: ");
        String password = sc.nextLine();

        //OUTPUT
        try {
            System.out.println("\n\u001B[34mPLAIN TEXT: \u001B[0m");
            System.out.println(UtilitiesAES.decrypt(text, password, true));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

//RESULT
/*
ENCRYPTED MESSAGE: RF441svPTZz/YDKHGkVoRBDevEeERIoih6OHosUfcUxrMXCL0JVswGNKTGaOFMSyNeGv/OoEA5yGe6rES8qfwAbQPKrfh6ihM1gRENlzG85EvpMz1ZR9/tguZq195ptmWkA1RM8o
PLAIN TEXT: A veure quan s'acaba el covid omicron i les seves variants
PASSWORD: Això és un password del 2022
*/
