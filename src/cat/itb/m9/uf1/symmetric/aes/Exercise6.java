package cat.itb.m9.uf1.symmetric.aes;

import java.util.Scanner;

public class Exercise6 {
    //MAIN
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //INPUT
        System.out.println("Enter an encrypted message to be decrypted with hexadecimal base: ");
        String text = sc.nextLine();
        System.out.println("Enter the password that was used when encrypting the message: ");
        String password = sc.nextLine();

        //OUTPUT
        try {
            System.out.println("\n\u001B[34mPLAIN TEXT: \u001B[0m");
            System.out.println(UtilitiesAES.decrypt(text, password, false));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

//RESULT
/*
ENCRYPTED MESSAGE: f4fd6ad48f782ecef75f2b50c065278f5fa5abf2ab2f758f16025f141b51545f62ea8be90a093f179f92d69c804ed6fcf21f408dea4380fa3b42e7dd8b169dd81129de98501d361225c41328ffddc0e57756f491dba7223d5f0ad7a5019c8c521b8727ca3fd208f5e73d511752cb04
PLAIN TEXT: En un any hem conegut vacunes com la Astra Zeneca, Pfizer o Moderna
PASSWORD: Això és un password del 2022
*/
