package cat.itb.m9.uf1.symmetric;

import java.util.Scanner;

public class Exercise1 {
    //MAIN
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter the message to decipher: ");
        String message = sc.nextLine();

        System.out.println();

        //CODE LOOP
        for (int i = 1; i < 25; i++) {
            StringBuilder plainText = new StringBuilder();

            for(int j = 0; j < message.length(); j++) {
                //CURRENT CHAR
                char c = message.charAt(j);
                boolean isUpperCase = Character.isUpperCase(c);

                //DECIPHERING
                if (Character.isLetter(message.charAt(j))) {
                    c = (char) (message.charAt(j) - i);

                    if (c < 'a' && !isUpperCase)
                        c = (char) (c + 26);
                    else if (c > 'z')
                        c = (char)(c - 26);
                    else if (c < 'A')
                        c = (char)(c + 26);
                    else if (c > 'Z' && isUpperCase)
                        c = (char)(c - 26);
                }

                //DECIPHERED TEXT
                plainText.append(c);
            }

            //OUTPUT
            System.out.printf("\u001B[35mCode:\u001B[0m %2d\u001B[35m, Plain Text:\u001B[0m %s\n", i, plainText);        }
    }
}

//RESULT
/*
ENCRYPTED MESSAGE: Jyx rjx ij Ufuf Stjq t ijqx Wjnx i'Twnjsy?
PLAIN TEXT: Ets mes de Papa Noel o dels Reis d'Orient?
CODE: 5
*/
