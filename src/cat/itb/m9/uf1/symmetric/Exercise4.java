package cat.itb.m9.uf1.symmetric;

import java.util.Scanner;

public class Exercise4 {
    //MAIN
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //INPUT
        System.out.println("Enter the message to decipher: ");
        String message = sc.nextLine();
        System.out.println("Enter the key:");
        int key = sc.nextInt();

        //NUMBER OF COLUMNS
        int cols = (int) Math.ceil((double) message.length() / key);

        //COLUMN
        int column = 0;

        //COUNTER
        int counter = 0;

        for (int i = 0; i < message.length(); i++) {
            System.out.print(message.charAt(counter));

            if (counter == cols * key - cols + column) {
                column++;
                counter = column;
            }
            else
                counter += cols;
        }
    }
}
